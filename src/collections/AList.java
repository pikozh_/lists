package collections;

import java.util.Arrays;

public class AList implements IList {

    private static final int CAPACITY = 10;

    private Integer[] arrayOfAlist;

    private static final Integer[] EMPTY_LIST = {};

    private int size;

    public AList() {
        this.arrayOfAlist = new Integer[CAPACITY];
    }


    public AList(int capacity) {
        if (capacity > 0) {
            this.arrayOfAlist = new Integer[capacity];
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " + capacity);
        }
    }

    public AList(int[] array) {
        this.arrayOfAlist = new Integer[array.length];
        for (int i = 0; i < array.length; i++){
            this.arrayOfAlist[i] = array[i];
            this.size++;
        }
    }

    @Override
    public void clear() {
        this.arrayOfAlist = EMPTY_LIST;
        this.size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index <= this.size) {
            return arrayOfAlist[index];
        } else {
            throw new IllegalArgumentException("Illegal index: " + index);
        }
    }

    @Override
    public boolean add(int number) {
        Integer[] temp = Arrays.copyOf(arrayOfAlist, (int) ((this.arrayOfAlist.length + 1) * 1.2));
        temp[size] = number;
        this.arrayOfAlist = temp;
        this.size++;
        return true;
    }

    @Override
    public boolean add(int index, int number) {
        if (index >= 0 && index < this.size) {

            Integer[] temp = new Integer[(int) ((this.arrayOfAlist.length + 1) * 1.2)];
            System.arraycopy(this.arrayOfAlist, 0, temp, 0, index);
            temp[index] = number;
            System.arraycopy(this.arrayOfAlist, index, temp, index + 1, this.size - index);

            this.arrayOfAlist = temp;
            this.size++;
            return true;
        } else {
            throw new IndexOutOfBoundsException("Illegal index: " + index);
        }
    }

    @Override
    public int remove(int number) {
        int searchableIndex = -1;
        for (int i = 0; i < size; i++) {
            if (this.arrayOfAlist[i] == number) {
                searchableIndex = i;
                break;
            }
        }
        if (searchableIndex != -1) {
            System.arraycopy(this.arrayOfAlist, searchableIndex + 1, this.arrayOfAlist, searchableIndex, this.arrayOfAlist.length - searchableIndex - 1);
            this.size--;
            return 1;
        } else {
            return searchableIndex;
        }
    }

    @Override
    public int removeByIndex(int index) {
        if (index >= 0 && index < this.size) {
//            System.arraycopy(this.arrayOfAlist, 0, this.arrayOfAlist, 0, index);
            System.arraycopy(this.arrayOfAlist, index + 1, this.arrayOfAlist, index, this.arrayOfAlist.length - index - 1);
            this.size--;
            return 1;
        } else {
            return -1;
        }
    }

    @Override
    public boolean contains(int number) {
        for (int i = 0; i < size; i++) {
            if (this.arrayOfAlist[i] == number) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {

        if (size == 0) {
            System.out.println("[]");
        } else {
            StringBuilder stringBuilder = new StringBuilder("[");
            for (int i = 0; i < size; i++) {
                stringBuilder.append(arrayOfAlist[i] + ", ");
            }
            stringBuilder.replace(stringBuilder.length() - 2, stringBuilder.length(), "]");
            System.out.println(stringBuilder);
        }
    }

    @Override
    public int[] toArray() {
        int[] result = new int[arrayOfAlist.length];
        for (int i = 0; i < size; i++) {
            result[i] = arrayOfAlist[i].intValue();
        }
        return result;
    }

    @Override
    public IList subList(int fromIdex, int toIndex) {
        AList list = new AList();
        for (int i = fromIdex; i < toIndex; i++) {
            list.add(this.arrayOfAlist[i]);
        }
        return list;
    }

    @Override
    public boolean removeAll(int[] arr) {
        for (int i = 0; i < this.size; i++) {
            for (int a : arr) {
                this.remove(a);
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(int[] arr) {
        Integer[] temp;
        if (arr.length > 10) {
            temp = new Integer[arr.length];
        } else {
            temp = new Integer[10];
        }

        int index = 0;
        for (int i = 0; i < this.size; i++) {
            for (int a : arr) {
                if (a == this.arrayOfAlist[i]) {
                    temp[index] = a;
                    index++;
                }
            }
        }
        this.arrayOfAlist = temp;
        return true;
    }

    @Override
    public String toString() {
        if (size == 0) {
            return "[]";
        } else {
            StringBuilder stringBuilder = new StringBuilder("[");
            for (int i = 0; i < size; i++) {
                stringBuilder.append(arrayOfAlist[i] + ", ");
            }
            stringBuilder.replace(stringBuilder.length() - 2, stringBuilder.length(), "]");
            return stringBuilder.toString();
        }
    }
}
