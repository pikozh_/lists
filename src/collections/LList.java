package collections;


public class LList implements IList {

    private Node head;
    private Node end;
    private int size;

    private static class Node {
        private int val;
        private Node next;

        public Node() {
        }

        public Node(int val) {
            this.val = val;
        }


    }

    public LList() {
    }

    public LList(int[] array){
        for (int element : array){
            add(element);
        }
    }


    @Override
    public void clear() {
        head = null;
        end = null;
        size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node result = head;
        for (int i = 0; i < index; i++) {
            result = result.next;
        }

        return result.val;
    }


    @Override
    public boolean add(int number) {
       return addLast(number);
    }

    @Override
    public boolean add(int index, int value) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node newNode = new Node(value);
        if (index == 0) {
            addStart(value);
        } else {
            Node previous = head;
            int count = 0;
            while (count < index - 1) {
                previous = previous.next;
                count++;
            }
            Node current = previous.next;
            newNode.next = current;
            previous.next = newNode;

        }
        size++;
        return true;
    }

    public boolean addStart(int value) {
        Node newNode = new Node(value);
        if (head == null) {
            head = newNode;

        } else {
            newNode.next = head;
            head = newNode;
        }
        size++;
        return true;
    }

    public boolean addLast(int value) {
        Node newNode = new Node(value);
        if (head == null) {
            head = newNode;
        } else {
            Node current = head;
            while (null != current.next) {
                current = current.next;
            }
            current.next = newNode;
        }
        size++;
        return true;
    }


    @Override
    public int remove(int number) {
        Node current = head;
        Node temp = null;
        if (current != null && current.val == number){
            head = current.next;
            return 1;
        }
        while (current != null && current.val != number){
            temp = current;
            current = current.next;
        }
        if (current == null){
            return -1;
        }
        temp.next = current.next;
        size--;
        return 1;

    }

    @Override
    public int removeByIndex(int index) {
        if (index < 0 || index > size - 1) {
            throw new IllegalArgumentException();
        }
        if (index == 0){
            Node temp = head;
            head = head.next;
            temp.next = null;
        }else {
            Node previous = head;
            int count = 0;
            while (count < index - 1){
                previous = previous.next;
                count++;
            }
            Node current = previous.next;
            previous.next = current.next;
            current.next = null;
        }
        size--;
        return 1;
    }

    @Override
    public boolean contains(int number) {
        for (int i = 0; i < size; i++) {
            if (get(i) == number) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        if (head == null) {
            System.out.print("[]");
        } else {
            StringBuilder stringBuilder = new StringBuilder("[");
            Node current = head;
            while (current != null) {
                stringBuilder.append(current.val + ", ");
                current = current.next;
            }
            stringBuilder.replace(stringBuilder.length() - 2, stringBuilder.length(), "]");
            System.out.println(stringBuilder);
        }
    }

    @Override
    public int[] toArray() {
        int[] array = new int[0];
        if (head == null) {
        } else {
            Node current = head;
            array = new int[size];
            int count = 0;
            while (current != null) {
                array[count] = current.val;
                current = current.next;
                count++;
            }
        }
        return array;
    }

    @Override
    public String toString() {
        if (head == null) {
            return "[]";
        } else {
            StringBuilder stringBuilder = new StringBuilder("[");
            Node current = head;
            while (current != null) {
                stringBuilder.append(current.val + ", ");
                current = current.next;
            }
            stringBuilder.replace(stringBuilder.length() - 2, stringBuilder.length(), "]");
            return stringBuilder.toString();
        }
    }

    @Override
    public IList subList(int fromIdex, int toIndex) {
        return null;
    }

    @Override
    public boolean removeAll(int[] arr) {
        return false;
    }

    @Override
    public boolean retainAll(int[] arr) {
        return false;
    }

}
