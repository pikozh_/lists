import collections.AList;
import collections.LList;


public class Main {
    public static void main(String[] args) {
        AList aList = new AList();
        aList.add(1);
        aList.add(2);
        aList.print();

        LList lList = new LList();
        lList.add(1);
        lList.add(2);
        lList.print();
    }
}
