import collections.LList;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class LListTest {
    @Test
    public void clearPass() {
        //given
        LList lList = new LList();
        lList.add(1);
        lList.add(2);

        //when
        lList.clear();
        String expected = "[]";
        String actual = lList.toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sizeShouldPass() {
        //given
        LList lList = new LList();
        lList.add(1);
        lList.add(2);

        //when
        int expected = 2;
        int actual = lList.size();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getShouldPass() {
        //given
        LList lList = new LList();
        lList.add(1);
        lList.add(2);

        //when
        int expected = 2;
        int actual = lList.get(1);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void addShouldPassTrue() {
        //given
        LList lList = new LList();

        //when
        boolean actual = lList.add(1);

        //then
        Assert.assertTrue(actual);
    }

    @Test
    public void addShouldPass() {
        //given
        int[] array = {1, 2, 3, 4};
        LList lList = new LList(array);

        //when
        int[] arrayForExpected = {1, 2, 3, 4, 5};
        String expected = Arrays.toString(arrayForExpected);
        lList.add(5);
        String actual = lList.toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void addWithIndexShouldPass() {
        //given
        int[] array = {1, 2, 3, 4};
        LList lList = new LList(array);

        //when
        int[] arrayForExpected = {1, 2, 10, 3, 4};
        String expected = Arrays.toString(arrayForExpected);
        lList.add(2, 10);
        String actual = lList.toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void removeShouldPass() {
        //given
        int[] array = {1, 2, 3, 4};
        LList lList = new LList(array);

        //when
        int[] arrayForExpected = {1, 2, 4};
        String expected = Arrays.toString(arrayForExpected);
        lList.remove(3);
        String actual = lList.toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void removeByIndexShouldPass() {
        //given
        int[] array = {1, 2, 3, 4};
        LList lList = new LList(array);

        //when
        int[] arrayForExpected = {1, 2, 4};
        String expected = Arrays.toString(arrayForExpected);
        lList.removeByIndex(2);
        String actual = lList.toString();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void containsShouldPass() {
        //given
        int[] array = {1, 2, 3, 4};
        LList aList = new LList(array);

        //when
        boolean actual = aList.contains(4);

        //then
        Assert.assertTrue(actual);
    }
}
